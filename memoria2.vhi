
-- VHDL Instantiation Created from source file memoria2.vhd -- 12:11:25 07/01/2022
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT memoria2
	PORT(
		clk : IN std_logic;
		en : IN std_logic;
		we : IN std_logic;
		rst : IN std_logic;
		addr : IN std_logic_vector(9 downto 0);
		di : IN std_logic_vector(15 downto 0);          
		do : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	Inst_memoria2: memoria2 PORT MAP(
		clk => ,
		en => ,
		we => ,
		rst => ,
		addr => ,
		di => ,
		do => 
	);


