----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:04:53 07/01/2022 
-- Design Name: 
-- Module Name:    TBmemorydemo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--LECCION 1
--LECCION 1
--LECCION 1

ENTITY TB_memoriarm IS
port (
salida: out std_logic_vector(1 downto 0);
direcccion: in std_logic_vector(1 downto 0));
END TB_memoriarm;


architecture arq of TB_memoriarm is
type datos is array (0 to 3) of std_logic_vector (1 downto 0);
constant rom : datos :=(
"11", "10", "00", "01"
);
begin
salida <= rom (to_integer (unsigned(direccion)));
end arq;